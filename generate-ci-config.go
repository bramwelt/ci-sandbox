package main

import (
	"os"
	"text/template"
)

type Job struct {
	Name      string
	Container string
}

var job_template = `{{ .Name }}-1:
  stage: build
  image: {{ .Container }}
  script: echo "hello"

{{ .Name }}-2:
  stage: build
  image: {{ .Container }}
  script: echo "hello"

{{ .Name }}-3:
  stage: build
  image: {{ .Container }}
  script: echo "hello"
`

func main() {
	u := Job{"run-container", "alpine"}

	ut, err := template.New("jobs").Parse(job_template)

	if err != nil {
		panic(err)
	}

	err = ut.Execute(os.Stdout, u)

	if err != nil {
		panic(err)
	}
}
